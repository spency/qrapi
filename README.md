## qrcode scanner api web app based on zbar

This is a docker container with zbar qrcode/barcode scanner and wrapped with node.js web app. 
You can deploy it to any docker container service. It acts as a microservice, it scans the uploaded image file, parse any barcode or qrcode in the image, and response with a json.
To upload the image, send a HTTP POST request with mutlipart encoded body and include image with the *image* field. 

`curl -F image=@IMG_7071.JPG http://localhost:8080`


