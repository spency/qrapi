(function() {
  var fs, process;

  fs = require('fs');

  process = require('child_process');

  module.exports = function(photo, callback) {
    var err, killed, stderr, stdout, zbarimg;
    stdout = '';
    stderr = '';
    killed = false;
    if ((photo == null) || (callback == null) || photo.length === 0) {
      err = new Error('Missing parameter');
      callback(err, null);
      return false;
    }
    zbarimg = process.spawn('zbarimg', [photo, '-q']);
    zbarimg.stdout.setEncoding('utf8');
    // zbarimg.stderr.setEncoding('utf8');
    zbarimg.stdout.on('data', function(data) {
      return stdout += data;
    });
    zbarimg.stderr.on('data', function(data) {
      return stderr += data;
    });
    zbarimg.on('error', function(err) {
      if (killed === true) {
        return false;
      }
      killed = true;
      callback(err, null);
      return true;
    });
    return zbarimg.on('close', function(code) {
      var codeName;
      if (killed === true) {
        return false;
      }
      killed = true;
      if (stdout != null) {
        callback(null, stdout);
        return true;
      }
      if (stderr != null) {
        err = new Error(stderr);
        callback(err, null);
        return false;
      }
    });
  };

}).call(this);
