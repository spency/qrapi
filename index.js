'use strict';

const express = require('express')
const greenlock = require('greenlock-express')
const cors = require('cors')
const multer = require('multer')
const fs = require('fs')
const https = require('https')
const zbarimg = require('./zbarimg')

const upload = multer({dest: '/tmp'})

const PORT = 8080
const SECURE_PORT = 8443

// App
const app = express()
app.use(cors())

function scan(filePath) {
  return new Promise((resolve, reject) => {
    zbarimg(filePath, function(err, code) {
      if (err) {
        reject(new Error(err))
      } else {
        var codes = code.split('\n').map(function(n) { return n.trim(); })
        var ret = {}

        codes.forEach(function(code) {
          var codeType = code.substring(0, code.indexOf(":"))
          if (codeType.length) {
            var codeValue = code.replace(codeType+":", "")
            if (!ret[codeType]) {
              ret[codeType] = []
            }
            ret[codeType].push(codeValue)
          }
        })
      
        resolve(ret)
      }
    })

  })
}

app.post('/', upload.single('image'), function(req, res, next) {
  if (!req.file) {
    res.status(500).end()
    return
  }

  scan(req.file.path).then(ret => {
    fs.unlink(req.file.path)
    res.json(ret)
  }).catch(err => {
    fs.unlink(req.file.path)
    res.status(500).end()
  })
})

app.get('/url/:url', function(req, res, next) {
  const url = decodeURIComponent(req.params.url)

  const filename = (new Date()).getTime() + ""
  const filePath = "/tmp/" + filename

  const file = fs.createWriteStream(filePath)

  file.on('close', () => {
    scan(filePath).then(ret => {
      fs.unlink(filePath)
      res.json(ret).end()
    }).catch(err => {
      fs.unlink(filePath)
      res.status(500).end()
    })
  })

  file.on('error', e => {
    console.error(e)
    res.status(500).end()
  })

  const request = https.get(url, function(download) {
    download.pipe(file)    
  }).on('error', e => {
    console.error(e)
    res.status(500).end()
  })
})


greenlock.create({
  server: 'https://acme-v01.api.letsencrypt.org/directory',
  email: 'sysheen@gmail.com',
  agreeTos: true,
  approveDomains: ['api.sobot.io'],
  app: app
}).listen(PORT, SECURE_PORT)

// app.listen(PORT)
console.log('Running on http://localhost:' + PORT)
